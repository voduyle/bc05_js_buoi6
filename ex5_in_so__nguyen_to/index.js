

function laSoNguyenTo(n) {
    if (n == 2) {
        return true;
    }
    if (n < 2) {
        return false;
    }
    if (n % 2 == 0) {
        return false;
    }
    for (var i = 3; i < n - 1; i += 2) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}
function inSo() {
    var soNguyenTo = "";
    var soN = document.getElementById("number").value * 1;
    for (var i = 0; i <= soN; i++) {
        if (laSoNguyenTo(i) == true) {
            soNguyenTo += i + " "
        }
    }
    document.getElementById("result").innerHTML = `Số nguyên tố: ${soNguyenTo}`
}